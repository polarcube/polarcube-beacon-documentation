# Polarcube Beaconing

The radio is based of the SPIRIT1 trancivier IC.

## Modulation

Beacons are modulated with 4800 baud FSK at 401.35MHz.  The freqeuncy deviation is +/-2.4kHz.  Packet format shown below.

|Name    |Length (bytes)|Description                                |
|--------|--------------|-------------------------------------------|
|Sync    |4             |0xAA 0xAA 0xAA 0xAA                        |
|Preamble|4             |0x88 0x88 0x88 0x88                        |
|Length  |1             |Length of the data field in bytes          |
|Data    |Var           |Packet data payload                        |
|CRC     |1             |CRC8 of length & data Poly: 0x07 Init: 0xFF|

## Data whitening

The data whitening is done with a 9 byte LSFR initalized with the below sequence.  The XOR of the msb in byte 0 and the msb in byte 5 is the next bit to shift in.  All the bits are shifted left and the XORed result of the aformentioned bits is set as the lsb in byte 8.  The whitening is reset for each packet and is applied from the length to CRC fields.

`
FF E1 1D 9A ED 85 33 24 EA
`

## Beacon types

Polarcube beacons come in 3 forms.

|Value|Description|
|-----|------------|
|0x00 |CDH Beacon  |
|0x01 |EPS Beacon  |
|0xFF |Haiku Beacon|

## CDH Beacons

These beacons start with an IP header of the following data to be stripped.

```
45 00 00 80 00 00 40 00 40 11 36 69 01 01 01 01
01 01 01 02 8b c1 8b ba 00 6c 67 eb
```

This is then followed by data of the below format.

|Name                      |Offset|Type    |Description                                           |
|--------------------------|------|--------|------------------------------------------------------|
|Packet Type               |28    |uint8_t |Fixed value 0x69 (Beacon)                             |
|Packet Type               |29    |uint8_t |Fixed value 0x69 (Not a typo this is here 2x)         |
|Beacon Type               |30    |uint8_t |Fixed value for FSW 0x00                              |
|Epoch Time                |31    |uint32_t|Time in seconds since Flight Software started         |
|System Time               |35    |uint32_t|Unix time seconds                                     |
|Reboot Count              |39    |uint32_t|Number of types system has rebooted                   |
|Satellite Mode            |43    |uint8_t |Current satelite mode, see FSWMode table below        |
|Current Mode Enter Time   |44    |uint32_t|Time current mode was entered in seconds in Epoch Time|
|Com Pass Count            |48    |uint32_t|Number of COM passes attempted                        |
|Memory                    |52    |float   |Percent of memory free                                |
|CPU1                      |56    |float   |Percent of CPU load over last 1 minute                |
|CPU5                      |60    |float   |Percent of CPU load over last 5 minutes               |
|CPU15                     |64    |float   |Percent of CPU laod over last 15 minutes              |
|Battery State of Charge   |66    |uint16_t|Battery remaning capacity in miliamp hours            |
|X Position                |74    |double  |GPS X-position in ECEF                                |
|Y Position                |82    |double  |GPS Y-position in ECEF                                |
|Z Position                |90    |double  |GPS Z-position in ECEF                                |
|X Velocity                |98    |double  |GPS X-velocity in ECEF                                |
|Y Velocity                |106   |double  |GPS Y-velocity in ECEF                                |
|Z Velocity                |114   |double  |GPS Z-velocity in ECEF                                |
|GPS Week                  |122   |int32_t |GPS week 0-1023                                       |
|GPS Second                |126   |float   |GPS second 0-604799                                   |
|RAD Number                |130   |uint16_t|Number of Radiometry Passes complete (Always 0)       |

### FSW Modes

|Mode                     |Value|
|-------------------------|-----|
|Bus                      |1    |
|Payload                  |2    |
|Com                      |3    |
|Reset                    |4    |
|Bus to Payload Transition|5    |
|Payload to Bus Transition|6    |
|Bus to Com Transition    |7    |
|Com to Bus Transition    |8    |
|Detumble ot Bus          |9    |
|Detumble                 |10   |
|ADS                      |11   |
|ADS to Bus               |12   |
|Bus to ADS               |13   |

## EPS (Electrical Power System) Beacons

When the CDH is in sleep mode becons are generated every 15 seconds by the EPS.  The messages have the below format.

|Name                      |Byte Offset|Source   |Type    |Description                                                |
|--------------------------|-----------|---------|--------|-----------------------------------------------------------|
|Routing                   |0          |EPS      |uint8_t |Fixed value 0x02                                           |
|Packet Type               |1          |EPS      |uint8_t |Fixed value 0x69 (Beacon)                                  |
|Beacon Type               |2          |EPS      |uint8_t |Fixed value 0x01                                           |
|3V3 Current               |3          |Hot Swaps|uint16_t|Current on 3V3 rail in counts                              |
|3V3 Voltage               |5          |Hot Swaps|uint16_t|Voltage of 3V3 rail in counts measured at EPS hotswap      |
|$`V_{bat}`$ Current         |7          |Hot Swaps|uint16_t|Current on $`V_{bat}`$ rail in counts                        |
|$`V_{bat}`$ Voltage         |9          |Hot Swaps|uint16_t|Voltage of $`V_{bat}`$ rail in counts measured at EPS hotswap|
|12V Current               |11         |Hot Swaps|uint16_t|Current on 12V rail in counts                              |
|12V Voltage               |13         |Hot Swaps|uint16_t|Voltage of 12V rail in counts measured at EPS hotswap      |
|Battery Remaining Capacity|15         |Battery  |uint16_t|Battery remaning capacity in miliamp hours                 |
|Battery Current           |17         |Battery  |int16_t |Battery current in mA                                      |
|Battery Voltage           |19         |Battery  |uint16_t|Battery voltage in mV                                      |
|Battery Status            |21         |Battery  |uint16_t|Battery status more information below                      |
|X- Solar Voltage          |23         |Backboard|uint16_t|Voltage Sense X- Solar Panel in mV                         |
|X+ Solar Voltage          |25         |Backboard|uint16_t|Voltage Sense X+ Solar Panel in mV                         |
|Y- Solar Voltage          |27         |Backboard|uint16_t|Voltage Sense Y- Solar Panel in mV                         |
|Y+ Solar Voltage          |29         |Backboard|uint16_t|Voltage Sense Y+ Solar Panel in mV                         |
|X- Solar Current          |31         |Backboard|uint16_t|Current Sense X- Solar Panel in mA                         |
|X+ Solar Current          |33         |Backboard|uint16_t|Current Sense X+ Solar Panel in mA                         |
|Y- Solar Current          |35         |Backboard|uint16_t|Current Sense Y- Solar Panel in mA                         |
|Y+ Solar Current          |37         |Backboard|uint16_t|Current Sense Y+ Solar Panel in mA                         |
|Solar Charge Current      |39         |Backboard|uint16_t|Current from MPPT to Battery in mA                         |
|Solar Charge Voltage      |41         |Backboard|uint16_t|Voltage from MPPT to Battery                               |
|PD Hor                    |43         |Backboard|uint16_t||
|                          |45         |Backboard|uint16_t||
|                          |45         |Backboard|uint16_t||
|                          |45         |Backboard|uint16_t||
|                          |45         |Backboard|uint16_t||
|                          |45         |Backboard|uint16_t||
|                          |45         |Backboard|uint16_t||
|                          |45         |Backboard|uint16_t||
|PD 45°                    |45         |Backboard|uint16_t||
|                          |45         |Backboard|uint16_t||
|                          |45         |Backboard|uint16_t||
|Z+ Photo Diode            |45         |Backboard|uint16_t||
|                          |45         |Backboard|uint16_t||
|                          |45         |Backboard|uint16_t||
|                          |45         |Backboard|uint16_t||
|                          |45         |Backboard|uint16_t||
|Gyro0 X                   |45         |Backboard|uint16_t||
|Gyro0 Y                   |45         |Backboard|uint16_t||
|Gyro0 Z                   |45         |Backboard|uint16_t||
|Gyro1 X                   |45         |Backboard|uint16_t||
|Gyro1 Y                   |45         |Backboard|uint16_t||
|Gyro1 Z                   |45         |Backboard|uint16_t||
|Mag0 X                    |45         |Backboard|uint16_t||
|Mag0 Y                    |45         |Backboard|uint16_t||
|Mag0 Z                    |45         |Backboard|uint16_t||
|Mag1 X                    |45         |Backboard|uint16_t||
|Mag1 Y                    |45         |Backboard|uint16_t||
|Mag1 Z                    |45         |Backboard|uint16_t||
|Backboard Temperature     |45         |Backboard|uint16_t|Temperature of backboard as measured by magnetometers|
|Backboard Status          |45         |Backboard|uint16_t|Status of backboard|
|Read Fault                |45         |Backboard|uint16_t|Indicates if data is valid from various sensors|

### Conversions

$`I = \frac{10.584}{4096}\frac{A}{count} \cdot counts`$

$`V = \frac{26.35}{4096}\frac{V}{count} \cdot counts`$

### Battery status

See BQ40Z50-R1 Technical reference manual 13.23 for bit field breakdown.

https://www.ti.com/lit/ug/sluubc1d/sluubc1d.pdf?ts=1609394478286

### Read Fault

Data is valid if the coresponding bit is low

|Bit|Description         |
|---|--------------------|
|0  |Hot Swap Data Valid |
|1  |Battery Data Valid  |
|2  |Backboard Data Valid|

## Haiku Beacon

This is a simple Haiku hardcoded into the EPS to be sent 1 in every 100 beacons.  For the joy of discovery I have not included the message here.

|Name       |Type   |Description               |
|-----------|-------|--------------------------|
|Routing    |uint8_t |Fixed value 0x02         |
|Packet Type|uint8_t |Fixed value 0x69 (Beacon)|
|Beacon Type|uint8_t |Fixed value 0xFF         |
|Message    |cstring |Contains Haiku           |
